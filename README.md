## install npm and node
Follow the link: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

## Install packages
- npm install

## Tools
- Bootstrap v4.6.0 (https://getbootstrap.com/)
- webpack ^5.22.0

## RUN
- npm run dev
- npm run prod
